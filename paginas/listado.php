<?php  
//Recojo primero los datos de la web
if(isset($_GET['campo'])){
	$campo=$_GET['campo'];
}else{
	$campo='nombreProd';
}

if(isset($_GET['orden'])){
	$orden=$_GET['orden'];
}else{
	$orden='ASC';
}
?>

<br>
<h4>Listado de Productos</h4>

<!-- MENU DE FILTRO POR CAMPO Y ORDEN -->
<!-- MENU DE FILTRO POR CAMPO Y ORDEN -->
<!-- MENU DE FILTRO POR CAMPO Y ORDEN -->

<ul class="nav nav-tabs">
	<?php  
	if($campo=='nombreProd'){
		if($orden=='ASC'){
			?>
			<li>
				<a href="index.php?p=listado.php&campo=nombreProd&orden=DESC">
					Nombre
					<span class="glyphicon glyphicon-arrow-up"></span>
				</a>
			</li>
			<?php
		}else{
			?>
			<li>
				<a href="index.php?p=listado.php&campo=nombreProd&orden=ASC">
					Nombre
					<span class="glyphicon glyphicon-arrow-down"></span>
				</a>
			</li>
			<?php
		}
	}else{
		?>
		<li>
			<a href="index.php?p=listado.php&campo=nombreProd&orden=ASC">
				Nombre
			</a>
		</li>
		<?php
	}
	?>
	
	<?php  
	if($campo=='precioProd'){
		if($orden=='ASC'){
			?>
			<li>
				<a href="index.php?p=listado.php&campo=precioProd&orden=DESC">
					Precio
					<span class="glyphicon glyphicon-arrow-up"></span>
				</a>
			</li>
			<?php
		}else{
			?>
			<li>
				<a href="index.php?p=listado.php&campo=precioProd&orden=ASC">
					Precio
					<span class="glyphicon glyphicon-arrow-down"></span>
				</a>
			</li>
			<?php
		}
	}else{
		?>
		<li>
			<a href="index.php?p=listado.php&campo=precioProd&orden=ASC">
				Precio
			</a>
		</li>
		<?php
	}
	?>

	<?php  
	if($campo=='unidadesProd'){
		if($orden=='ASC'){
			?>
			<li>
				<a href="index.php?p=listado.php&campo=unidadesProd&orden=DESC">
					Unidades
					<span class="glyphicon glyphicon-arrow-up"></span>
				</a>
			</li>
			<?php
		}else{
			?>
			<li>
				<a href="index.php?p=listado.php&campo=unidadesProd&orden=ASC">
					Unidades
					<span class="glyphicon glyphicon-arrow-down"></span>
				</a>
			</li>
			<?php
		}
	}else{
		?>
		<li>
			<a href="index.php?p=listado.php&campo=unidadesProd&orden=ASC">
				Unidades
			</a>
		</li>
		<?php
	}
	?>

	<?php  
	if($campo=='fechaAlta'){
		if($orden=='ASC'){
			?>
			<li>
				<a href="index.php?p=listado.php&campo=fechaAlta&orden=DESC">
					Fecha
					<span class="glyphicon glyphicon-arrow-up"></span>
				</a>
			</li>
			<?php
		}else{
			?>
			<li>
				<a href="index.php?p=listado.php&campo=fechaAlta&orden=ASC">
					Fecha
					<span class="glyphicon glyphicon-arrow-down"></span>
				</a>
			</li>
			<?php
		}
	}else{
		?>
		<li>
			<a href="index.php?p=listado.php&campo=fechaAlta&orden=ASC">
				Fecha
			</a>
		</li>
		<?php
	}
	?>
</ul>

<!-- FIN DEL MENU DE FILTRO POR CAMPO Y ORDEN -->
<!-- FIN DEL MENU DE FILTRO POR CAMPO Y ORDEN -->
<!-- FIN DEL MENU DE FILTRO POR CAMPO Y ORDEN -->

<?php  
switch($campo){
	case 'nombreProd':
		$nombreMostrar='Nombre de producto';
		break;
	case 'precioProd':
		$nombreMostrar='Precio de producto';
		break;
	case 'unidadesProd':
		$nombreMostrar='Unidades de producto en stock';
		break;
	case 'fechaAlta':
		$nombreMostrar='Fecha de alta de producto';
		break;
}

switch($orden){
	case 'ASC':
		$ordenMostrar='Ascendente';
		break;
	case 'DESC':
		$ordenMostrar='Descendente';
		break;
}
?>

<h5>Ordenado por <strong><?php echo $nombreMostrar;?></strong> de forma <strong><?php echo $ordenMostrar;?></strong></h5>

<?php
//pregunta
//$sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT 0,10";
//$sql="SELECT * FROM productos ORDER BY idProd DESC";

//$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY idProd DESC";

$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY $campo $orden";

//ejecutar la consulta
$consulta=mysqli_query($conexion, $sql);
//muestro resultados
while($r=mysqli_fetch_array($consulta)){
	?>
	<article>
		<header>
			<h3>
				<?php echo $r['nombreProd'];?>
				<small><?php echo $r['precioProd'];?> Euros</small>
				<span class="badge">
					<?php echo $r['unidadesProd'];?> Unds
				</span>
				 - <a href="index.php?p=detalle.php&id=<?php echo $r['idProd'];?>">
				 	Ver Producto
				 </a>
				 - <a href="index.php?p=borrar.php&id=<?php echo $r['idProd'];?>" onCLick="if(!confirm('Estas seguro')){return false;};">
				 	Borrar Producto
				 </a>
				 - <a href="index.php?p=modificar.php&id=<?php echo $r['idProd'];?>">
				 	Modificar Producto
				 </a>
			</h3>
			<h4>Categoria: <?php echo $r['nombreCat'];?></h4>
		</header>
		<section>
			<?php echo $r['descripcionProd'];?>
		</section>
		<footer>
			Disponible desde <?php echo $r['fechaAlta'];?>
		</footer>
	</article>
	<?php
}
?>