<?php  

// includes/classes/imagen.class.php
Class Imagen{
	public $archivo;
	public $ancho;
	public $alto;
	public $redondeada; //Redondeada o no

	function __construct($archivo, $ancho="100%", $alto=""){
		$this->archivo=$archivo;
		$this->ancho=$ancho;
		$this->alto=$alto;
		$this->redondeada=true; //Esta redondeada
	}

	function dibujaImagen(){
		if($this->redondeada==true){
			$c='img-rounded';
		}else{
			$c='';
		}

		$resultado='';
		$resultado='<img src="'.$this->archivo.'" width="'.$this->ancho.'" height="'.$this->alto.'" class="'.$c.'">';
		return $resultado;
	}
}

?>